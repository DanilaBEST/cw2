package itis.parsing2;

import itis.parsing2.annotations.Concatenate;
import itis.parsing2.annotations.NotBlank;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FactoryParsingServiceImpl implements FactoryParsingService {

    @Override
    public Factory parseFactoryData(String factoryDataDirectoryPath) throws FactoryParsingException, IOException, IllegalAccessException {
        final String[] title = new String[1];
        final String[] firstName = new String[1];
        final String[] secondName = new String[1];
        final String[] middleName = new String[1];
        final String[] description = new String[1];
        final Long[] amountOfWorkers = new Long[1];
        List<String> departments = new ArrayList<>();
        String orgFullName = "";
        Files.walk(Paths.get(factoryDataDirectoryPath))
                .forEach(file -> {
                    if (!Files.isDirectory(file)) {
                        try {
                            String line = "";
                            BufferedReader bufferedReader = new BufferedReader(new FileReader(file.toFile().getAbsolutePath()));
                            while ((line = bufferedReader.readLine()) != null) {

                                if (!line.equals("---")) {
                                    line = line.replaceAll("\"", "");
                                    String[] strings = line.split(":");
                                    if (strings[0].equals("departments")) {
                                        line = line.replaceAll(",", "");
                                        line = line.replaceAll("\\[]", "");// хз как эт пофиксить
                                        line = line.replaceAll("]", "");
                                        departments.addAll(Arrays.asList(strings).subList(1, strings.length));
                                    }
                                    if (strings[0].equals("title")) {
                                        title[0] = strings[1];
                                    }
                                    if (strings[0].equals("firstName")) {
                                        firstName[0] = strings[1];
                                    }
                                    if (strings[0].equals("middleName")) {
                                        middleName[0] = strings[1];
                                    }
                                    if (strings[0].equals("secondName")) {
                                        secondName[0] = strings[1];
                                    }
                                    if (strings[0].equals("amountOfWorkers")) {
                                        amountOfWorkers[0] = Long.parseLong(strings[1]);
                                    }
                                    if (strings[0].equals("description")) {
                                        description[0] = strings[1];
                                    }
                                }
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                });
        orgFullName += firstName[0] + " ";
        orgFullName += middleName[0] + " ";
        orgFullName += secondName[0];
        Factory factory = new Factory();
        Field[] fields = factory.getClass().getDeclaredFields();
        for (Field field : fields) {
            field.setAccessible(true);
            Annotation[] annotations = field.getDeclaredAnnotations();
            if (field.getName().equals("title")) {
                field.set(factory, title[0]);
            }
            if (field.getName().equals("description")) {
                field.set(factory, description[0]);
            }
            if (field.getName().equals("amountOfWorkers")) {
                field.set(factory, amountOfWorkers[0]);
            }
            if (field.getName().equals("departments")) {
                field.set(factory, departments);
            }
            if (field.isAnnotationPresent(Concatenate.class)) {
                orgFullName = orgFullName.replaceAll(" ", field.getAnnotation(Concatenate.class).delimiter());
                field.set(factory, orgFullName);
            }
            if (field.isAnnotationPresent(NotBlank.class)) {
                if (field.get(factory) == null && field.get(factory) != "") {
                    //И ТУТ ВЫЛЕТАЕТ EXCEPTION///
                }
            }
        }
        return factory;
    }
}